package com.example.autoai;


import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import org.tensorflow.lite.Interpreter;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

public class TFLite {
    private int previousEvent = -1;

    public static MappedByteBuffer loadModelFile(Context context, String modelName) throws IOException {

        // Open the model using an input stream and memory map it to load
        AssetFileDescriptor fileDescriptor = context.getAssets().openFd(modelName + ".tflite");
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    public TupleStringInt binarizePredicitionArray(float[][] predictions){
        float max = predictions[0][0];
        int maxIndex = 0;

        //Finding the maximum
        for (int i=1;i<predictions[0].length;i++) {
            if (predictions[0][i] > max) {
                max = predictions[0][i];
                maxIndex = i;
            }
        }

        // Setting the maximum value of the array to a binary value of 1 and the rest to 0
        for (int i = 0;i<predictions[0].length;i++) {
            if (i == maxIndex) {
                predictions[0][i] = 1;
            } else {
                predictions[0][i] = 0 ;
            }
        }

        String probabilityColour;

        // -1 - error
        // 0 - low probability
        // 1 - medium probability
        // 2 - high probability

        // Checking if the previous event was the same as the current one detected
        if (this.previousEvent == maxIndex){
            this.previousEvent = maxIndex;
            if (max <= 0.5) {
                // Yellow
                probabilityColour = "#DED947";
            } else if (0.5 <= max && max <= 0.75) {
                // Orange
                probabilityColour = "#FFA500";
            } else if (max > 0.75) {
                // Red
                probabilityColour = "#FF0000";
            } else {
                // Black
                probabilityColour = "#000000";
            }
        } else {
            // Grey
            this.previousEvent = maxIndex;
            probabilityColour = "#C0C0C0";
        }


        TupleStringInt returnObj = new TupleStringInt();
        returnObj.colour = probabilityColour;
        returnObj.predicatedEvent = maxIndex;

        return returnObj;

    }

    public TupleStringInt predictEventType(Context context, float[][] input1D, float[][][] input3D, String modelName, boolean model3D){
        float[][] output = new float[1][7];
        Interpreter tflite;

        try {
            tflite = new Interpreter(TFLite.loadModelFile(context,modelName));
            if(model3D) {
                tflite.run(input3D,output);
            } else {
                tflite.run(input1D,output);

            }

            tflite.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return this.binarizePredicitionArray(output);
    }
}
