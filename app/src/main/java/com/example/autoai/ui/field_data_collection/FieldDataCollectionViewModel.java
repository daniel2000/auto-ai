package com.example.autoai.ui.field_data_collection;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class FieldDataCollectionViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public FieldDataCollectionViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is dashboard fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}