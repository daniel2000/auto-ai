package com.example.autoai.ui.field_data_collection;

import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.autoai.CSV;
import com.example.autoai.R;
import com.example.autoai.SensorsRunnable;

import java.time.LocalDate;
import java.time.LocalTime;

public class FieldDataCollectionFragment extends Fragment {

    private FieldDataCollectionViewModel fieldDataCollectionViewModel;

    TextView main_text;
    TextView sub_text;
    TextView countdown_text;
    ImageView image_view;
    CardView bottom_card;

    String[] eventPrompts = new String[7];
    int[] eventPromptImages = new int[7];
    int currentEventNo = 0;
    Button main_btn;

    Thread sensorsThread;
    SensorsRunnable sensorsRunnable;
    CSV csvHandler = new CSV();


    private Boolean training_finished = false;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fieldDataCollectionViewModel =
                new ViewModelProvider(this).get(FieldDataCollectionViewModel.class);
        View root = inflater.inflate(R.layout.fragment_field_data_collection, container, false);

        main_text = (TextView) root.findViewById(R.id.sub_text_eval);
        sub_text = (TextView) root.findViewById(R.id.main_text_eval);
        countdown_text = (TextView) root.findViewById(R.id.countdown_text);
        image_view = (ImageView) root.findViewById(R.id.image_view);
        bottom_card = (CardView) root.findViewById(R.id.cardView2);

        requireActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        main_btn = (Button) root.findViewById(R.id.main_btn);



        eventPrompts[0] = "Nonaggressive";
        eventPrompts[1] = "Right Turn";
        eventPrompts[2] = "Left Turn";
        eventPrompts[3] = "Acceleration";
        eventPrompts[4] = "Braking";
        eventPrompts[5] = "Left Lane Change";
        eventPrompts[6] = "Right Lane Change";

        eventPromptImages[0] = R.drawable.check;
        eventPromptImages[1] = R.drawable.right_turn;
        eventPromptImages[2] = R.drawable.left_turn;
        eventPromptImages[3] = R.drawable.accelerate;
        eventPromptImages[4] = R.drawable.brake;
        eventPromptImages[5] = R.drawable.left_lane_change;
        eventPromptImages[6] = R.drawable.right_lane_change;



        //sensorsRunnable = new SensorsRunnable(requireActivity().getApplicationContext(),getActivity(),20000);
        //sensorsThread = new Thread(sensorsRunnable);

        //sensorsThread.start();


        main_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTraining(view);
            }
        });

        return root;
    }


    public void startTraining(View view) {
        new Thread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void run() {

                changeButtonVisibility(false);
                changeTextVisibility(true);

                changeImage(currentEventNo);

                startCountAnimation(eventPrompts[currentEventNo]);

                try {
                    // 4000 milliseconds to wait for the countdown timer
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // Start of event
                String temp = LocalDate.now() + "," + LocalTime.now() + "," + System.nanoTime() + "," + currentEventNo + "\n";
                csvHandler.writeToCSV(requireActivity().getApplicationContext(),"Events/Events" + LocalDate.now()+ ".csv",temp);

                try {
                    // 5000 milliseconds for the event to occur
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // End of the event
                temp = LocalDate.now() + "," + LocalTime.now() + "," + System.nanoTime() + ",-" + currentEventNo + "\n";
                csvHandler.writeToCSV(requireActivity().getApplicationContext(),"Events/Events" + LocalDate.now() +".csv",temp);


                if (currentEventNo == 0) {
                    setButtonText("Continue");
                }

                changeButtonVisibility(true);
                changeTextVisibility(false);

                if(currentEventNo == 6) {
                    currentEventNo = -1;
                    setButtonText("Data Collection finished. Press to restart");
                }
                currentEventNo++;

            }
        }).start();
    }


    private void displayMessage(String message) {
        requireActivity().runOnUiThread(new Runnable() {
            public void run() {
                main_text.setText(message);
            }
        });
    }

    private void setButtonText(String message) {
        requireActivity().runOnUiThread(new Runnable() {
            public void run() {
                main_btn.setText(message);
            }
        });
    }


    private void changeButtonVisibility(Boolean visible) {
        requireActivity().runOnUiThread(new Runnable() {
            public void run() {
                if(visible){
                    main_btn.setVisibility(View.VISIBLE);
                    main_btn.setEnabled(visible);
                    main_text.setVisibility(View.INVISIBLE);
                    sub_text.setVisibility(View.INVISIBLE);
                } else {
                    main_btn.setVisibility(View.INVISIBLE);
                    main_btn.setEnabled(visible);
                    main_text.setVisibility(View.VISIBLE);
                    sub_text.setVisibility(View.VISIBLE);

                }
            }
        });
    }

    private void changeTextVisibility(Boolean visible) {
        requireActivity().runOnUiThread(new Runnable() {
            public void run() {
                if(visible){
                    main_text.setVisibility(View.VISIBLE);
                    sub_text.setVisibility(View.VISIBLE);
                } else {
                    main_text.setVisibility(View.INVISIBLE);
                    sub_text.setVisibility(View.INVISIBLE);
                }
            }
        });
    }


    private void changeImage(int currentEventNo) {
        requireActivity().runOnUiThread(new Runnable() {
            public void run() {
                image_view.setImageResource(eventPromptImages[currentEventNo]);
            }
        });
    }

    private void startCountAnimation(String eventPrompt) {
        requireActivity().runOnUiThread(new Runnable() {
            public void run() {

                new CountDownTimer(4000, 1000) {


                    public void onTick(long millisUntilFinished) {
                        countdown_text.setText("In " + millisUntilFinished / 1000 + " seconds");
                        main_text.setText(eventPrompt);
                    }

                    public void onFinish() {
                        countdown_text.setText("");
                        main_text.setText(eventPrompt.toUpperCase());
                    }
                }.start();

            }
        });


    }
}