package com.example.autoai.ui.evaluation;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.autoai.CSV;
import com.example.autoai.R;
import com.example.autoai.SensorsRunnable;
import com.example.autoai.TFLite;
import com.example.autoai.TupleArrayInt;
import com.example.autoai.TupleStringInt;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EvaluationFragment extends Fragment {

    private EvaluationViewModel evaluationViewModel;
    Thread predictionsThread;
    Thread sensorsThread;
    boolean predictionsThreadRun = true;
    SensorsRunnable sensorsRunnable;
    CSV csvHandler = new CSV();
    int lastRowRead = 0;
    String[] eventPrompts = new String[7];
    int[] eventPromptImages = new int[8];

    TextView main_text;
    TextView sub_text;
    ImageView image_view;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        evaluationViewModel =
                new ViewModelProvider(this).get(EvaluationViewModel.class);
        View root = inflater.inflate(R.layout.fragment_evaluation, container, false);



        /*
        final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        */

        main_text = (TextView) root.findViewById(R.id.main_text_eval);
        sub_text = (TextView) root.findViewById(R.id.sub_text_eval);
        image_view = (ImageView) root.findViewById(R.id.image_view_eval);

        sensorsRunnable = new SensorsRunnable(requireActivity().getApplicationContext(),getActivity(),80000);
        sensorsThread = new Thread(sensorsRunnable);

        sensorsThread.start();

        TrainingPredictionsRunnable predictionsRunnable = new TrainingPredictionsRunnable(sensorsRunnable);
        predictionsThread = new Thread(predictionsRunnable);
        predictionsThread.start();

        eventPrompts[0] = "Non-aggressive";
        eventPrompts[1] = "Right Turn";
        eventPrompts[2] = "Left Turn";
        eventPrompts[3] = "Acceleration";
        eventPrompts[4] = "Braking";
        eventPrompts[5] = "Left Lane Change";
        eventPrompts[6] = "Right Lane Change";

        eventPromptImages[0] = R.drawable.check;
        eventPromptImages[1] = R.drawable.right_turn;
        eventPromptImages[2] = R.drawable.left_turn;
        eventPromptImages[3] = R.drawable.accelerate;
        eventPromptImages[4] = R.drawable.brake;
        eventPromptImages[5] = R.drawable.left_lane_change;
        eventPromptImages[6] = R.drawable.right_lane_change;
        eventPromptImages[7] = R.drawable.detecting;

        requireActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        return root;
    }

    public class TrainingPredictionsRunnable implements Runnable {
        private SensorsRunnable sensorSet;
        public TrainingPredictionsRunnable(SensorsRunnable sensorSet) {
            this.sensorSet = sensorSet;
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        public void run() {

            TFLite tensorFlowLiteMethods = new TFLite();
            String output;

            TupleStringInt returnObj = new TupleStringInt();

            while (predictionsThreadRun) {

                returnObj.predicatedEvent = -1;

                TupleArrayInt arrayInput = new TupleArrayInt();
                List<Integer> results = new ArrayList<>();


                for (int i = 0; i < 32; i++) {
                    arrayInput = csvHandler.readCSV("Accelerometer/ACCELEROMETER" + LocalDate.now() + ".csv", requireActivity().getApplicationContext(), lastRowRead);

                    if (lastRowRead + 50 == arrayInput.lastRow) {
                        lastRowRead = arrayInput.lastRow - 49;
                        returnObj = tensorFlowLiteMethods.predictEventType(requireActivity().getApplicationContext(), null, arrayInput.array3D, "driver_behaviour_dataset50lag_acc_data", true);


                        output = LocalDate.now() + "," + LocalTime.now() + "," + System.nanoTime() + "," + arrayInput.array3D[0][0][0] + "," + arrayInput.array3D[0][0][1] + "," + arrayInput.array3D[0][0][2] + "," + returnObj.predicatedEvent + "\n";


                        csvHandler.writeToCSV(getActivity().getApplicationContext(), "AllPredictions/AllPredications" + LocalDate.now() + ".csv", output);

                        results.add(returnObj.predicatedEvent);
                    } else {
                        //Array is not filled up and hence neither will the rest going forward
                        break;
                    }

                }


                if (results.size() > 0) {
                    returnObj = mostCommon(results);
                }


                if (returnObj.predicatedEvent != -1 && returnObj.colour != null) {

                    changeImage(returnObj.predicatedEvent);
                    eventDetectedText(returnObj.predicatedEvent);


                    // Delay
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }



                    changeImage(7);
                    removeDetectedText();
                }
            }

        }

    }

    public static TupleStringInt mostCommon(List<Integer> list) {
        //https://stackoverflow.com/questions/19031213/java-get-most-common-element-in-a-list
        Map<Integer, Integer> map = new HashMap<>();

        for (Integer t : list) {
            Integer val = map.get(t);
            map.put(t, val == null ? 1 : val + 1);
        }

        Map.Entry<Integer, Integer> max = null;


        for (Map.Entry<Integer, Integer> e : map.entrySet()) {
            if (max == null || e.getValue() > max.getValue()) {
                max = e;
            }
        }

        Float probability = 0.0f;
        probability = (float) max.getValue()/list.size();

        String probabilityColour = "";


        if (probability <= 0.5) {
            // Yellow
            probabilityColour = "#DED947";
        } else if (0.5 <= probability && probability <= 0.75) {
            // Orange
            probabilityColour = "#FFA500";
        } else if (probability > 0.75) {
            // Red
            probabilityColour = "#FF0000";
        } else {
            // Black
            probabilityColour = "#000000";
        }



        TupleStringInt returnObj = new TupleStringInt();
        returnObj.colour = probabilityColour;
        returnObj.predicatedEvent =  max.getKey();

        return returnObj;
    }

    private void changeImage(int currentEventNo) {
        requireActivity().runOnUiThread(new Runnable() {
            public void run() {
                image_view.setImageResource(eventPromptImages[currentEventNo]);
            }
        });
    }

    private void eventDetectedText(int currentEventNo) {
        requireActivity().runOnUiThread(new Runnable() {
            public void run() {
                main_text.setText(eventPrompts[currentEventNo]);
                sub_text.setVisibility(View.VISIBLE);
                sub_text.setText("Detected");
            }
        });
    }

    private void removeDetectedText(){
        requireActivity().runOnUiThread(new Runnable() {
            public void run() {
                main_text.setText("Detecting...");
                sub_text.setVisibility(View.INVISIBLE);

            }
        });
    }

}