package com.example.autoai;


import androidx.appcompat.app.AppCompatActivity;

import android.hardware.SensorEventListener;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class TrainingProcedure extends AppCompatActivity {

    TextView main_text;
    private Boolean storeData = false;
    private int arrayCounter;
    private float[][] prevlinearAccData1D = new float[1][150];
    private float[][][] prevlinearAccData3D = new float[1][50][3];
    boolean model3D = false;
    String[] eventPrompts = new String[7];
    int currentEventNo = 0;
    Button main_btn;
    SensorEventListener accEventListener;
    Thread predictionsThread;
    Thread sensorsThread;
    SensorsRunnable sensorsRunnable;
    boolean predictionsThreadRun = true;
    CSV csvHandler = new CSV();
    int modelType = 0;

    int lastRowRead = 0;

    float prevX = 0;
    float prevY = 0;
    float prevZ = 0;

    TextView listLengthText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Driving Behaviour Monitor");
        setContentView(R.layout.fragment_field_data_collection);
        main_text = (TextView) findViewById(R.id.sub_text_eval);

        arrayCounter = 49;
        /*
        if (model3D) {
            arrayCounter = 49;
        } else {
            arrayCounter = 147;
        }

         */


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        main_btn = (Button) findViewById(R.id.main_btn);


        eventPrompts[0] = "Drive normally";
        eventPrompts[1] = "Hard Right";
        eventPrompts[2] = "Hard Left";
        eventPrompts[3] = "Accelerate";
        eventPrompts[4] = "Brake";
        eventPrompts[5] = "Change to Left Lane";
        eventPrompts[6] = "Change to Right Lane";


        //sensorsRunnable = new SensorsRunnable(getApplicationContext(),ge);
        //sensorsThread = new Thread(sensorsRunnable);

        //sensorsThread.start();

    }




}

