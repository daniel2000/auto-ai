package com.example.autoai;

public class SamplingPeriods {
    // 50Hz sampling rate. ie 20000 microseconds
    // 50Hz = 1000000 microseconds/ 20000 microseconds
    private final int evaluationSamplingPeriod = 20000;
    private int dataCollectionSamplingPeriod = 20000;

    public int getDataCollectionSamplingPeriod() {
        return dataCollectionSamplingPeriod;
    }

    public void setDataCollectionSamplingPeriod(int dataCollectionSamplingPeriod) {
        this.dataCollectionSamplingPeriod = dataCollectionSamplingPeriod;
    }
}
